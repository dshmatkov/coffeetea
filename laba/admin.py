from django.contrib import admin
from .models import Coffee, Review, Tea, Visitor, Wafer

# Register your models here.

admin.site.register(Coffee)
admin.site.register(Tea)
admin.site.register(Review)
admin.site.register(Visitor)
admin.site.register(Wafer)
