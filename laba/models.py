from __future__ import unicode_literals
import datetime

from django.db import models
from django.contrib.auth.models import User


class Tea(models.Model):
    name = models.CharField(max_length=200)
    description = models.TextField()
    color = models.CharField(max_length=100)
    origin = models.CharField(max_length=100)


class Coffee(models.Model):
    name = models.CharField(max_length=200)
    description = models.TextField()
    composition = models.CharField(max_length=200)
    origin = models.CharField(max_length=100)


class Wafer(models.Model):
    name = models.CharField(max_length=200)
    callories = models.IntegerField(default=0)
    dough = models.CharField(max_length=100)
    cream = models.CharField(max_length=100)
    description = models.TextField()
    origin = models.CharField(max_length=100)
    best_with_coffee = models.ForeignKey(Coffee, on_delete=models.SET_NULL, null=True)
    best_with_tea = models.ForeignKey(Tea, on_delete=models.SET_NULL, null=True)


class Visitor(User):
    discount = models.IntegerField(default=0)
    backend = 1


class Review(models.Model):
    user = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)
    text = models.TextField()
    mark = models.IntegerField(default=3)
    published = models.DateTimeField(default=datetime.datetime.now())
