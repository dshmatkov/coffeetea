from django.contrib.auth import login, authenticate, logout
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from django.shortcuts import render, redirect, get_object_or_404
from django.template import loader, RequestContext
from django.http import HttpResponse, HttpResponseRedirect
from models import Coffee, Review, Tea, Visitor, Wafer

OFFSET = 50


def index(request):
    last_review = Review.objects.order_by('-published')[:5]
    context = RequestContext(request, {
        'last_review': last_review
    })
    return render(request, 'laba/index.html', context)


@login_required
def add_review(request):
    if request.method == "GET":
        return render(request, 'laba/review_form.html')
    elif request.method == "POST":
        mark = request.POST['mark']
        text = request.POST['text']
        user = request.user
        review = Review(mark=mark, text=text, user=user)
        review.save()
        return HttpResponseRedirect(reverse('index'))


def register(request):
    if request.method == 'GET':
        if not request.user.is_authenticated():
            return render(request, 'laba/register.html')
        else:
            return HttpResponse('You have already been authentificated!', status=400)
    if request.method == 'POST':
        username = request.POST['username']
        email = request.POST['email']
        password = request.POST['password']
        user = Visitor.objects.create_user(username, email, password)
        try:
            user.save()
        except Exception as e:
            return HttpResponse(e.message)
        login(request, user)
        return HttpResponseRedirect(reverse('index'))
    return HttpResponse('Something wrong with your request', status=400)


def log_in(request):
    if request.method == 'GET':
        return render(request, 'laba/login.html')
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username=username, password=password)
        if user and user.is_active:
            login(request, user)
            return HttpResponseRedirect(reverse('index'))
    return HttpResponse('Something wrong!', status=400)


def log_out(request):
    logout(request)
    return HttpResponseRedirect(reverse('index'))


# objects
@login_required
def all_tea(request):
    tea = Tea.objects.order_by('name')
    return render(request, 'laba/tea_list.html', {'tea': tea})


@login_required
def tea_details(request, tea_id):
    tea = get_object_or_404(Tea, pk=tea_id)
    return render(request, 'laba/tea.html', {'tea': tea})


@login_required
def all_coffee(request):
    coffee = Coffee.objects.order_by('name')
    return render(request, 'laba/coffee_list.html', {'coffee': coffee})


@login_required
def coffee_details(request, coffee_id):
    coffee = get_object_or_404(Coffee, pk=coffee_id)
    return render(request, 'laba/coffee.html', {'coffee': coffee})


@login_required
def all_wafers(request):
    wafer = Wafer.objects.order_by('name')
    return render(request, 'laba/wafer_list.html', {'wafer': wafer})


@login_required
def wafers_details(request, wafer_id):
    tea = get_object_or_404(Tea, pk=wafer_id)
    return render(request, 'laba/wafer.html', {'tea': tea})


def all_review(request):
    review = Review.objects.order_by('-published')
    return render(request, 'laba/reviews.html', {'review': review})
