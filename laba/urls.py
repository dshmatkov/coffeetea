from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^add_review/$', views.add_review, name='add_review'),
    url(r'^register/$', views.register, name='register'),
    url(r'^login/', views.log_in, name='login'),
    url(r'^accounts/login/', views.log_in, name='login'),
    url(r'^logout/$', views.log_out, name='logout'),
    # object_urls
    url(r'^tea/$', views.all_tea, name='teas'),
    url(r'^tea/(?P<tea_id>[0-9]+)$', views.tea_details, name='tea'),
    url(r'^coffee/$', views.all_coffee, name='coffees'),
    url(r'^coffee/(?P<coffee_id>[0-9]+)$', views.coffee_details, name='coffee'),
    url(r'^wafers/$', views.all_wafers, name='wafers'),
    url(r'^wafers/(?P<wafers_id>[0-9]+)$', views.all_wafers, name='wafers'),
    url(r'^reviews/$', views.all_review, name='reviews')
]
